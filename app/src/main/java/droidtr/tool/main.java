package droidtr.tool;

import android.app.*;
import android.content.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.google.android.gms.ads.*;
import java.util.*;

public class main extends Activity {
	
	Integer ws = 0;
	Integer hs = 0;
	float ds = 0;
	int toast = Toast.LENGTH_LONG;
	boolean premium = false;
	boolean sysless = false;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		int rootModeReceiver = getIntent().getIntExtra("rootType",2);
		if(rootModeReceiver >= 2 || rootModeReceiver < 0){
			new shell().sendToastNotification(getBaseContext(),"Please start this activity on rootctl activity!",Toast.LENGTH_LONG);
			finish();
		} else {
			if(rootModeReceiver == 0) sysless = false;
			else sysless = true;
		}
		try{ premium = new shell().findPackage("droidtr.tool.premium"); } catch(Exception e){}
		if(Build.VERSION.SDK_INT == 19) (findViewById(R.id.sb)).setVisibility(View.VISIBLE);
		if(!premium) ((AdView) findViewById(R.id.adView1)).loadAd(new AdRequest.Builder().build());
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		ws = dm.widthPixels;
		ds = dm.density * 160;
		if(dm.heightPixels > 2400) hs = 2560;
		if(dm.heightPixels > 1800 && dm.heightPixels < 2400) hs = 1920;
		if(dm.heightPixels > 1000 && dm.heightPixels < 1800) hs = 1280;
		if(dm.heightPixels > 700 && dm.heightPixels < 1000) hs = 800;
		
		((EditText) findViewById(R.id.wsize)).setText(ws+"");
		((EditText) findViewById(R.id.hsize)).setText(hs+"");
		((EditText) findViewById(R.id.density)).setText(ds+"");
		((Button) findViewById(R.id.reboot)).setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				Intent i = new Intent(main.this,reboot.class);
				startActivity(i);
			}
		});
		
		((Button) findViewById(R.id.format)).setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				startActivity(new Intent(main.this,format.class));
			}
		});
		
		((Button) findViewById(R.id.hardfreeze)).setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				hardfreeze();
			}
		});
		
		((Button) findViewById(R.id.sleep)).setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				sleep();
			}
		});
		
		((Button) findViewById(R.id.resetOs)).setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				setOverscan(false);
			}
		});
		
		((Button) findViewById(R.id.setOverscan)).setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				setOverscan(true);
			}
		});
		
		((Button) findViewById(R.id.resetsize)).setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				setsize(false);
			}
		});
		
		Button b1 = (Button) findViewById(R.id.pre1);
		Button b2 = (Button) findViewById(R.id.pre2);
		Button b3 = (Button) findViewById(R.id.pre3);
		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) b3.setVisibility(View.VISIBLE);
		else b3.setVisibility(View.GONE);
		
		if(premium){
			
			b1.setText("Security Addon");
			b1.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					Intent i = new Intent(main.this,secaddon.class);
					startActivity(i);
				}
			});
			
			b2.setText("Clean ROM");
			b2.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View a){
					Intent i = new Intent(main.this,cleanrom.class);
					startActivity(i);
				}
			});
			
			b3.setText("Quick Settings Tile Set");
			b3.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					startActivity(new Intent(main.this,qstileset.class));
				}
			});
			
        } else {
			
			b1.setText("???");
			b1.setEnabled(false);
			b2.setText("???");
			b2.setEnabled(false);
			b3.setText("???");
			b3.setEnabled(false);
			
		}
		
		((Button) findViewById(R.id.setsize)).setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				setsize(true);
			}
		});
			
		((Button) findViewById(R.id.rmdir)).setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				rmdir();
			}
		});

		((Button) findViewById(R.id.sysinfo)).setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View a){
				Intent i = new Intent(main.this,sysinfo.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
				startActivity(i);
			}
		});
   
		((Button) findViewById(R.id.resetdensity)).setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View a){
				setDensity(false);
			}
		});
		
		((Button) findViewById(R.id.setdensity)).setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View a){
				setDensity(true);
			}
		});
		
	}

	void setDensity(boolean a){
		try{
			if(a){
				new shell().exec(1,"wm density "+((EditText) findViewById(R.id.density)).getText().toString()+"");
				new shell().sendToastNotification(getBaseContext(),"Density applied",toast);
			} else {
				new shell().exec(1,"wm density reset");
				new shell().sendToastNotification(getBaseContext(),"Density resetting...",toast);
				((EditText) findViewById(R.id.density)).setText(ds+"");
			}
		} catch(Exception e){
			new shell().sendToastNotification(getBaseContext(),"Density set process failed",toast);
		}
	}
	
	void setOverscan(boolean a){
		try{
			if(a){
				new shell().exec(1,"wm overscan "+((EditText) findViewById(R.id.os1)).getText().toString()+","+
								((EditText) findViewById(R.id.os2)).getText().toString()+","+
								((EditText) findViewById(R.id.os3)).getText().toString()+","+
								((EditText) findViewById(R.id.os4)).getText().toString()+"");
				new shell().sendToastNotification(getBaseContext(),"Overscan applied",toast);
			} else {
				new shell().exec(1,"wm overscan reset");
				new shell().sendToastNotification(getBaseContext(),"Overscan resetting...",toast);
				((EditText) findViewById(R.id.os1)).setText("0");
				((EditText) findViewById(R.id.os2)).setText("0");
				((EditText) findViewById(R.id.os3)).setText("0");
				((EditText) findViewById(R.id.os4)).setText("0");
			}
		} catch(Exception e){
			new shell().sendToastNotification(getBaseContext(),"Overscan set process failed",toast);
		}
	}
	
	void sleep(){
		startActivity(new Intent(main.this,droidtr.tool.shortcuts.fsleep.class));
	}
	
    void hardfreeze(){
        try {
			String s1 = new shell().execForStringOutput(1,"ls /data/data/ | grep -v droidtr.tool | grep -v com.android.systemui | grep -x -v android | echo $(cat)");
			String[] s2 = s1.split(" ");
			for(int i=0;i!=s2.length;i++)
					new shell().exec(1,"am force-stop "+s2[i]);
			new shell().sendToastNotification(getBaseContext(),"Force freeze all apps",toast);
        }catch (Exception e){
            new shell().sendToastNotification(getBaseContext(),"Force freeze all apps process failed",toast);
        }
    }

    void setsize(boolean a){
        try {
			if(a){
				new shell().exec(1,"wm size "+((EditText) findViewById(R.id.wsize)).getText()+
								"x"+((EditText) findViewById(R.id.hsize)).getText()+"");
				new shell().sendToastNotification(getBaseContext(),"Setting screen size...",toast);
			} else {
				new shell().exec(1,"wm size reset");
				new shell().sendToastNotification(getBaseContext(),"Resetting screen size...",toast);
				((EditText) findViewById(R.id.wsize)).setText(ws+"");
				((EditText) findViewById(R.id.hsize)).setText(hs+"");
			}
        }catch (Exception e){
            new shell().sendToastNotification(getBaseContext(),"Set Screen Size Failed",toast);
        }
    }
	
	void rmdir(){
		try{
			new shell().exec(1,"find /sdcard/ > /data/data/droidtr.tool/cache/list ;"+
							 " sed -i \"s|^|rmdir \\\"|\" /data/data/droidtr.tool/cache/list ;"+
							 " sed -i \"s|$|\\\"|\" /data/data/droidtr.tool/cache/list ;"+
							 " sh /data/data/droidtr.tool/cache/list ;"+
							 " rm -f /data/data/droidtr.tool/cache/list");
			new shell().sendToastNotification(getBaseContext(),"Removed Empty Directory",toast);
		} catch(Exception e){
			new shell().sendToastNotification(getBaseContext(),"Remove empty directory process failed",toast);
		}
	}
	
}
