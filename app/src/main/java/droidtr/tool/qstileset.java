package droidtr.tool;

import android.os.*;
import android.app.*;
import android.widget.*;
import android.widget.LinearLayout.*;
import android.view.*;
import android.text.*;

public class qstileset extends Activity {
	int p = 16;
	@Override
	public void onCreate(Bundle b){
		super.onCreate(b);
		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
		LinearLayout ll = new LinearLayout(this);
		ll.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setGravity(Gravity.CENTER_VERTICAL);
		if(Build.VERSION.SDK_INT == 19) ll.setPadding(p,p+25,p,p);
		else ll.setPadding(p,p,p,p);
		TextView tv = new TextView(this);
		tv.setLayoutParams(lp);
		tv.setText("QS Tile Number");
		tv.setGravity(Gravity.CENTER_HORIZONTAL);
		final EditText et = new EditText(this);
		et.setLayoutParams(lp);
		et.setHint("XX");
		et.setText(getValue());
		et.setSingleLine();
		et.setSelection(et.getText().toString().length());
		et.setGravity(Gravity.CENTER_HORIZONTAL);
		et.setFilters(new InputFilter[] {new InputFilter.LengthFilter(2)});
		et.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
		View vw = new View(this);
		vw.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,p));
		Button bt = new Button(this);
		bt.setLayoutParams(lp);
		bt.setText("Set");
		bt.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				try{
					new shell().exec(1,"settings put secure sysui_qqs_count "+et.getText().toString());
				} catch(Exception e){}
			}
		});
		ll.addView(tv);
		ll.addView(et);
		ll.addView(vw);
		ll.addView(bt);
		ll.setOnTouchListener(new View.OnTouchListener(){
			@Override
			public boolean onTouch(View p1, MotionEvent p2){
				
				return false;
			}
		});
		setContentView(ll);
	}
	
	public String getValue(){
		try{
			return new shell().execForStringOutput(1,"settings get secure sysui_qqs_count");
		} catch(Exception e){ return null; }
	}
	
}
