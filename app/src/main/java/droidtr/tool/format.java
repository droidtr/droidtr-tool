package droidtr.tool;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import java.io.*;
import java.util.*;

import java.lang.Process;

public class format extends Activity {
	public String[] modlar = {"User Apps","System Data","System + User Data","Other Data's\n(without app,data,media,dalvik-cache)","Cache","Dalvik-cache","Internal Storage"};
	@Override
	public void onCreate(Bundle b){
		super.onCreate(b);
		int p = 26;
		View v = new View(this);
		v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 16));
		LinearLayout ll = new LinearLayout(this);
		ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
		ll.setPadding(p,p,p,p);
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setBackgroundColor(getResources().getColor(R.color.arkaplan));
		TextView tv = new TextView(this);
		tv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
		tv.setText("Please don't use 'System Data' tick on GApps included ROM.");
		tv.setTextColor(getResources().getColor(R.color.textColor));
		ll.addView(tv);
		for (int i=0;i!=7;i++){
			View bosluk = new View(this);
			bosluk.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,10));
			CheckBox cb = new CheckBox(this);
			cb.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			cbEkle(cb,i);
			ll.addView(bosluk);
			ll.addView(cb);
		}
		Button btn = new Button(this);
		btn.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		btn.setText("Clear");
		btn.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				temizle();
			}
		});
		ll.addView(v);
		ll.addView(btn);
		setContentView(ll);
	}

	public void cbEkle(CheckBox cb, final int i){
		cb.setText(modlar[i]);
		cb.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton c, boolean b){
				if(b) secim[i] = true;
				else secim[i] = false;
			}
		});
	}
	
	public boolean[] secim = {false,false,false,false,false,false,false};

	public void temizle(){
		try{
			if(secim[0]){
				new shell().exec(1,"rm -rf /data/app/*");
				new shell().exec(1,"rm -rf /sdcard/.android_secure");
				new shell().exec(1,"rm -rf "+new shell().getExternalMounts()+"/.android_secure");
			}
			if(secim[1]) {
				new shell().exec(1,"rm -rf /data/data/com.android.*");
				new shell().exec(1,"rm -rf /data/data/com.google.android.*");
			}
			if(secim[2]) new shell().exec(1,"rm -rf /data/data/*");
			if(secim[3]) new shell().exec(1,"rm -rf /data/$(ls /data/ | grep -v app | grep -v data | grep -v media)");
			if(secim[4]) {
				new shell().exec(1,"rm -rf /data/data/*/cache");
				new shell().exec(1,"rm -rf /sdcard/Android/data/*/cache");
				new shell().exec(1,"rm -rf /sdcard/DCIM/.thumbnails/");
				new shell().exec(1,"rm -rf /cache/*");
			}
			if(secim[5]) new shell().exec(1,"rm -rf /data/dalvik-cache/*");
			if(secim[6]) new shell().exec(1,"rm -rf /data/media/*");
		} catch(Exception e){}
	}
}
