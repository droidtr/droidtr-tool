package droidtr.tool;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;

public class reboot extends Activity {
	public String[] modlar = {"Reboot","Soft Reboot"/* 1","Soft Reboot 2","Soft Reboot 3","Soft Reboot 4","Soft Reboot 5","Soft Reboot 6"/*,"Soft Reboot 7","Soft Reboot 8"*/,"Recovery","Bootloader"};
	public String[] rebcmd = {"reboot","killall zygote","setprop ctl.restart zygote",
								"stop zygote ; start zygote","killall surfaceflinger",
								"stop surfaceflinger ; start surfaceflinger","am restart",
								"reboot recovery","reboot bootloader"};
	@Override
	public void onCreate(Bundle b){
		super.onCreate(b);
		int i = 0;
		int p = 26;
		LinearLayout ll = new LinearLayout(this);
		ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
		ll.setPadding(p,p,p,p);
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setBackgroundColor(getResources().getColor(R.color.arkaplan));
		while(true){
			if (i > 3){
				i = 0;
				break;
			} else {
				View bosluk = new View(this);
				bosluk.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,10));
				Button btn = new Button(this);
				btn.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
				butonEkle(btn,i);
				ll.addView(bosluk);
				ll.addView(btn);
				i++;
			}
		}
		setContentView(ll);
	}

	public void butonEkle(Button btn, final int i){
		btn.setText(modlar[i]);
		btn.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				modsec(i);
			}
		});
	}

	public void modsec(int i){
		int g = 1;
		try {
			if (i == 0) new shell().exec(1,rebcmd[0]);
			if (i == 1) new shell().exec(1,rebcmd[g]);
			if (i == 2) new shell().exec(1,rebcmd[7]);
			if (i == 3) new shell().exec(1,rebcmd[8]);
		} catch(Exception e){
			if(g <= rebcmd.length-2) {
				g++;
				modsec(1);
			}
		}
	}
}
