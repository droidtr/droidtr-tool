package droidtr.tool;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.os.*;
import android.telephony.*;
import android.view.*;
import android.widget.*;
import android.widget.LinearLayout.*;
import java.io.*;

import java.lang.Process;

public class sysinfo extends Activity {
	
	public Integer p = 16;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
		LayoutParams lp = new LayoutParams(
			LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		LayoutParams tp = new LayoutParams(
			LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);
		TextView t1 = new TextView(this);
		TextView t2 = new TextView(this);
		LinearLayout l1 = new LinearLayout(this);
        l1.setOrientation(LinearLayout.HORIZONTAL);
        l1.setLayoutParams(lp);
		l1.setBackgroundColor(getResources().getColor(R.color.arkaplan));
		l1.setGravity(Gravity.CENTER);
        t1.setLayoutParams(tp);
		t1.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		t1.setTextSize(16);
		t1.setTextColor(getResources().getColor(R.color.textColor));
		t2.setLayoutParams(tp);
		t2.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
		t2.setTextSize(16);
		t2.setTextColor(getResources().getColor(R.color.yazi));
		l1.setPadding(p,p,p,p);
        l1.addView(t1);
		l1.addView(t2);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(l1);
		t1.setText("ssh \nbash \nbusybox \nsftp \nntfs \nxposed \ngapps \nrom \nsdk \nkernel \nselinux \nmimari \ndonanım \nmarka \nbuild \noperator ");
		t2.setText(islem(false,true,"ssh")+"\n"+islem(false,true,"bash")+"\n"+
				   islem(false,true,"busybox")+"\n"+islem(false,true,"sftp")+"\n"+islem(false,false,"/system/lib/libntfs-3g.so")+
				   "\n"+islem(false,false,"/system/framework/XposedBridge.jar")+"\n"+islem(true,false,"")+"\n"+bProp(true)
				   +"\n"+Build.VERSION.SDK_INT+" ("+Build.VERSION.RELEASE+")"+"\n"+kSurumu()+"\n"+selinux()+"\n"+cpuMimarisi()
				   +"\n"+bProp(false)+"\n"+Build.BRAND+"\n"+Build.ID+"\n"+operator());
    }

	public String operator(){
		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		String s1 = tm.getSimOperatorName();
		String s2 = tm.getNetworkOperatorName();
		if(s1.contains(s2)) return s1;
		else {
			if(s1 != null && s1 != "") return s1;
			else {
				if(s2 != null && s2 != "") return s2;
				else return "Yok ✖";
			}
		}
	}
	
	public String bProp(boolean mod){
		String temp = "AOSP";
		if(mod){
			try{
				String output1 = new shell().execForStringOutput(0,"ls /system/etc/permissions/ | grep cyanogenmod");
				String output2 = new shell().execForStringOutput(0,"ls /system/etc/permissions/ | grep zenui");
				String output3 = new shell().execForStringOutput(0,"ls /system/etc/permissions/ | grep touchwiz");
				String output4 = new shell().execForStringOutput(0,"ls /system/etc/permissions/ | grep lge");
				String output5 = new shell().execForStringOutput(0,"ls /system/etc/ | grep miui");
				if(output1.length() > 3){
					if((new shell().getBuildPropValue("ro.rr.version")).length()>1){
						temp = "RR";
					}else{
						if(Build.VERSION.SDK_INT>23){
							temp = "LOS";
						} else {
							temp = "CM";
						}
					}
				} else {
					if(output2.length() > 3)temp = "ZenUI";
					else {
						if(output3.length() > 3) temp = "TW";
						else {
							if(output4.length() > 3) temp = "HomeUX";
							else {
								if(output5.length() > 3) temp = "MIUI";
								else temp = "AOSP";
							}
						}
						
					}
				}
				String a = new shell().getBuildPropValue("ro.build.display.id");
				String[] b = a.split("-");
				String[] c = b[0].split("_");
				String[] d = c[0].split(" ");
				return d[0].toUpperCase().replaceAll("İ","I")+" ("+temp+")";
			} catch (Exception e){ return ""; }
		} else {
			return Build.HARDWARE+" ("+Build.DEVICE+")";
		}
	}

	public String cpuMimarisi(){
		String[] a = (Build.CPU_ABI).split("-");
		String[] b = (Build.CPU_ABI2).split("-");
		if(a[0].contains(b[0])){
			if(a[0].contains("arm")) return "ARM-"+a[1];
			else return a[0];
		} else {
			if(b[0].contains("arm")) return a[0]+" ARM";
			else return a[0]+" "+b[0];
		}

	}

	public String selinux(){
		try { return new shell().getSELinux(0); }
		catch (Exception e){ return ""; }
	}

	public String kSurumu(){
		try {
			String s = new shell().execForStringOutput(0,"uname -r");
			String[] v1 = s.toString().split("-");
			String[] v2 = v1[0].split("_");
			String[] v3 = v2[0].split(" ");
			return v3[0];
		} catch (Exception e) { return ""; }
	}

	public String islem(boolean g, boolean b, String s){
		if(b){
			if(new shell().checkFileInSysPath(s))
				return "Var ✔";
			else return "Yok ✖";
		} else {
			if(g){
				try{
					if(new shell().findPackage("com.google.android.gms"))
						return "Var ✔";
					else return "Yok ✖";
				} catch(Exception e){
					return "Yok ✖";
				}
				
			} else {
				if((new File(s)).exists()) return "Var ✔";
				else return "Yok ✖";
			}
		}
	}
	
}
