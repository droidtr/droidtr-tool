package droidtr.tool;

import android.app.*;
import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.widget.LinearLayout.*;

public class rootctl extends Activity{
	LinearLayout ll;
	Button bt;
	TextView tv;
	@Override
	public void onCreate(Bundle b){
		super.onCreate(b);
		setTitle("RootControl");
		ll = new LinearLayout(this);
		ll.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setPadding(16,16,16,16);
		ll.setGravity(Gravity.CENTER);
		tv = new TextView(this);
		tv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
		tv.setGravity(Gravity.CENTER_HORIZONTAL);
		bt = new Button(this);
		bt.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
		bt.setVisibility(View.GONE);
		ll.addView(tv);
		ll.addView(bt);
		setContentView(ll);
		checkRoot();
	}
	
	public void checkRoot(){
		tv.setText("Checking root permission...");
		try{
			int cr = new shell().checkRootAccess();
			if(cr == 0){
				tv.setText("System root detected!\n\nYou're welcome our app!\n\nATTENTION!\nIf your device\'s Android version is bigger than KitKat(19), please don\'t use \"Force Sleep\"!");
				new Handler().postDelayed(new Runnable(){
					@Override
					public void run(){
						Intent i = new Intent(rootctl.this,main.class);
						i.putExtra("rootType",0);
						startActivity(i);
						finish();
					}
				},5000);
			}
			if(cr == 1){
				tv.setText("Systemless root detected!\n\nYou're welcome our app!\n\nATTENTION!\nIf your device really has systemless root, please don\'t use \"Force Sleep\" and some premium options!");
				new Handler().postDelayed(new Runnable(){
					@Override
					public void run(){
						Intent i = new Intent(rootctl.this,main.class);
						i.putExtra("rootType",1);
						startActivity(i);
						finish();
					}
				},5000);
			}
			if(cr == 2){
				tv.setText("Check root operation failed!\n\nPlease click \"Retry\" button and try again.");
				bt.setText("Retry");
				bt.setOnClickListener(new View.OnClickListener(){
					@Override
					public void onClick(View v){
						bt.setVisibility(View.GONE);
						checkRoot();
					}
				});
				bt.setVisibility(View.VISIBLE);
			}
		} catch(Exception e){ checkRoot(); }
	}
	
}
