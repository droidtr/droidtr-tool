package droidtr.tool;

import android.app.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import java.io.*;
import java.net.*;

public class secaddon extends Activity {
	public int length = 0;
	public String file ="/data/data/droidtr.tool/cache/droidtr";
	public static String url = "https://raw.githubusercontent.com/parduscix/Guvenli_Internet/master/hosts";
	@Override
	public void onCreate(Bundle b){
		super.onCreate(b);
		int p = 26;
		int i = 0;
		String[] str = {"Activate","Remove"};
		LinearLayout ll = new LinearLayout(this);
		ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
		ll.setPadding(p,p,p,p);
		ll.setOrientation(LinearLayout.VERTICAL);
		TextView t = new TextView(this);
		t.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
		t.setTextSize(30);
		t.setGravity(Gravity.CENTER_HORIZONTAL);
		t.setTextColor(getResources().getColor(R.color.textColor));
		t.setText("\nSecurity Addon\nThis is help you for secure net\n");
		ll.addView(t);
		while(true){
			if(i > 1){
				i = 0;
				break;
			} else {
				View bosluk = new View(this);
				bosluk.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,10));
				ll.addView(bosluk);
				Button btn = new Button(this);
				btn.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
				btn.setText(str[i]);
				butonEkle(btn,i);
				ll.addView(btn);
				i++;
			}
		}
		setContentView(ll);
	}
	
	public void butonEkle(Button btn, final int i){
		if(activated()){
			if(i <= 1 && i >= 0){
				if(i == 0) btn.setEnabled(false);
				if(i == 1) btn.setEnabled(true);
			}
		} else {
			if(i == 0) btn.setEnabled(true);
			if(i == 1) btn.setEnabled(false);
		}
		btn.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				mod(i);
			}
		});
	}
	
	public void mod(int i){
		if(i <= 1 && i >= 0){
			if(i == 0) try{ new DownloadFileFromURL().execute(url); } catch(Exception e){}
			if(i == 1) try{ replaceFile(); } catch(Exception e){}
		}
	}

    class DownloadFileFromURL extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
			new shell().sendToastNotification(getBaseContext(),"Downloading required file...",Toast.LENGTH_LONG);
        }
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                int lenghtOfFile = conection.getContentLength();
				length = lenghtOfFile;
                InputStream input = new BufferedInputStream(url.openStream(),8192);
                OutputStream output = new FileOutputStream(file);
                byte data[] = new byte[1024];
                while ((count = input.read(data)) != -1) {
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
            } catch (Exception e) { Log.e("Error: ", e.getMessage()); }
            return null;
        }

        @Override
        protected void onPostExecute(String file_url) {
			new shell().sendToastNotification(getBaseContext(),"Replacing this file with empty required file...",Toast.LENGTH_LONG);
			replaceFile();
			new shell().sendToastNotification(getBaseContext(),"Security Addon is activated",Toast.LENGTH_LONG);
        }
    }
	
	public void replaceFile(){
		try{
			new shell().exec(1,"mount -o remount,rw /system");
			if(activated()){
				new shell().exec(1,"rm -f /system/etc/hosts");
				new shell().exec(1,"mv /system/etc/hosts.bak /system/etc/hosts");
				new shell().sendToastNotification(getBaseContext(),"Security Addon is deactivated",Toast.LENGTH_LONG);
			} else {
				new shell().exec(1,"mv "+file+" /system/etc/droidtr");
				new shell().exec(1,"mv /system/etc/hosts /system/etc/hosts.bak");
				new shell().exec(1,"mv /system/etc/droidtr /system/etc/hosts");
				new shell().exec(1,"chmod 644 /system/etc/hosts");
			}
			finish();
		} catch(Exception e){}
	}
	
	public boolean activated(){
		if((new File("/system/etc/hosts")).length()>1024) return true;
		else return false;
	}
	
}
