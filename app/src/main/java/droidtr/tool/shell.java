package droidtr.tool;

import android.app.*;
import android.content.*;
import android.telephony.*;
import android.widget.*;
import java.io.*;
import java.lang.reflect.*;
import java.util.*;

public class shell extends Activity {
	public int exec(int shellType,String command) throws IOException {
		String st = new String();
		if (shellType == 0) st = "sh";
		if (shellType == 1) st = "su";
		if (shellType == 2) st = "bash";
		if (command == "" || command == null) return 1;
		if (shellType > 2 || shellType < 0) return 1;
		exec(st,command);
		return 0;
	}

	public int exec(String customShell,String command) throws IOException {
		if (customShell != "" && customShell != null && command != "" && command != null) {
			java.lang.Process p = Runtime.getRuntime().exec(customShell);
			DataOutputStream dos = new DataOutputStream(p.getOutputStream());
			dos.writeBytes(command+"\n");
			dos.flush();
			dos.close();
			return 0;
		} else return 1;
	}
	
	public String execForStringOutput(int shellType,String command) throws IOException {
		String st = new String();
		if (shellType == 0) st = "sh";
		if (shellType == 1) st = "su";
		if (shellType == 2) st = "bash";
		if (shellType > 2 || shellType < 0) return null;
		return execForStringOutput(st,command);
	}
	
	public String execForStringOutput(String customShell,String command) throws IOException {
		if (customShell != "" && customShell != null && command != "" && command != null) {
			String line;
			StringBuilder s = new StringBuilder();
			java.lang.Process p = Runtime.getRuntime().exec(customShell);
			DataOutputStream dos = new DataOutputStream(p.getOutputStream());
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			dos.writeBytes(command+"\n");
			dos.flush();
			dos.close();
			while ((line = input.readLine()) != null) s.append(line);
			input.close();
			return s.toString();
		} else return null;
	}
	
	public int execForReturnCodeOutput(int shellType, String command) throws IOException {
		if(shellType <=2 && shellType >= 0 && command != "" && command != null)
			return Integer.parseInt(execForStringOutput(shellType, command+" > /dev/null 2>&1 ; echo $?"));
		else return 999;
	}
	
	public int execForReturnCodeOutput(String customShell, String command) throws IOException {
		if(customShell != "" && customShell != null && command != "" && command != null)
			return Integer.parseInt(execForStringOutput(customShell, command+" > /dev/null 2>&1 ; echo $?"));
		else return 999;
	}
	
	public boolean findPackage(String packageName) throws IOException {
			if(execForStringOutput(0,"pm list packages "+packageName).length() > 3)
				return true;
			else return false;
	}
	
	public boolean mountModeDetector(String partitionName, boolean rw) throws IOException {
		String temp1 = new String();
		String temp2 = execForStringOutput(1,"mount | grep "+partitionName);
		if(rw) temp1 = "rw";
		else temp1 = "ro";
		if(temp2.contains(partitionName) &&  temp2.contains(temp1))
			return true;
		else return false;
	}
	
	public int checkRootAccess() throws InterruptedException,IOException {
		exec(1,"mount -o remount,rw /system");
		if(mountModeDetector("system",true))
			return 0;
		else {
			new shell().exec(1,"touch /data/check");
			Thread.sleep(1500);
			if(checkFile("/data/check",0) == 0){
				exec(1,"rm -rf /data/check");
				return 1;
			} else return 2;
		}
	}
	
	public boolean checkActivityIsRunOrNot(Class activity){
		ActivityManager activityManager = (ActivityManager) getBaseContext().getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);
		for (ActivityManager.RunningTaskInfo task:tasks)
			if (activity.getCanonicalName().equalsIgnoreCase(task.baseActivity.getClassName()))
				return true;
		return false;
	}
	
	public boolean checkFileInSysPath(String fileName){
       String[] split = System.getenv("PATH").split(":");
        for (String file : split) {
            File f = new File(file,fileName);
            if (f.exists()) {
                return true;
            }
        }
        return false;
    }
	
	public long checkFile(String fileName,int option){
		File f = new File(fileName);
		if(option == 0){
			if(f.exists()) return 0;
			else return 1;
		} if(option == 1){
			if(f.exists()){
				if(f.isDirectory()) return 0;
				if(f.isFile()) return 1;
			} else return 2;
		} if(option == 2) return f.length();
		return 8;
	}
	
	public String getBuildPropValue(String request) 
		throws ClassNotFoundException,NoSuchMethodException,
				InvocationTargetException,IllegalAccessException {
		return (Class.forName("android.os.SystemProperties")
			.getMethod("get", String.class).invoke(null, request)).toString();
	}
	
	public String getSELinux(int lang) throws IOException {
		String l1 = new String();
		String l2 = new String();
		String l3 = new String();
		if(lang == 0){
			l1 = "Serbest";
			l2 = "Zorunlu";
			l3 = "Kapalı";
		} else {
			l1 = "Permissive";
			l2 = "Enforcing";
			l3 = "Disabled";
		}
		String s = new shell().execForStringOutput(0,"getenforce");
		if(s.toString().contains("Per")) return l1;
		if(s.toString().contains("Enf")) return l2;
		else {
			s = new shell().execForStringOutput(1,"getenforce");
			if(s.toString().contains("Per")) return l1;
			if(s.toString().contains("Enf")) return l2;
			return l3;
		}
	}
	
	public int setSELinux(int value){
		if(value <= 1 || value >= 0){
			try{
				exec(1,"setenforce "+value);
				return 0;
			} catch(Exception e){ return 1; }
		} else return 2;
	}
	
	public String getSystemSetting(String settingType,String settingName) throws IOException {
		return execForStringOutput(0,"settings get "+settingType+" "+settingName);
	}
	
	public boolean changeSystemSetting(String settingType,String settingName,String value) 
										throws InterruptedException,IOException {
		exec(1,"settings put "+settingType+" "+settingName);
		Thread.sleep(500);
		if(getSystemSetting(settingType, settingName) == value) return true;
		else return false;
	}
	
	public void sendToastNotification(Context context,String label,int timeMS){
		Toast.makeText(context,label,timeMS).show();
	}
	
	public void grantPermission(String packageName,String permissionName) throws IOException {
		exec(1, "pm grant "+packageName+" "+permissionName);
	}
	
	public void sendStatusbarNotification(String label,boolean enabled,int icon,int priority){
		NotificationManager nMN = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		if(enabled){
			Notification n  = new Notification.Builder(this)
				.setContentTitle(getResources().getString(R.string.app_name))
				.setContentText(label)
				.setSmallIcon(icon)
				.setPriority(priority)
				.build();
			nMN.notify(1, n);
		} else nMN.cancel(1);
	}
	
	public String getExternalMounts() {
		final HashSet<String> out = new HashSet<String>();
		String reg = "(?i).*vold.*(vfat|ntfs|exfat|fat32|ext3|ext4).*rw.*";
		StringBuilder s = new StringBuilder();
		try {
			final Process process = new ProcessBuilder().command("mount")
                .redirectErrorStream(true).start();
			process.waitFor();
			final InputStream is = process.getInputStream();
			final byte[] buffer = new byte[1024];
			while (is.read(buffer) != -1) s.append(new String(buffer));
			is.close();
		} catch (final Exception e){ e.printStackTrace(); }
		final String[] lines = s.toString().split("\n");
		for (String line : lines) {
			if (!line.toLowerCase(Locale.US).contains("asec")) {
				if (line.matches(reg)) {
					String[] parts = line.split(" ");
					for (String part : parts) {
						if (part.startsWith("/"))
							if (!part.toLowerCase(Locale.US).contains("vold"))
								out.add(part);
					}
				}
			}
		}
		return out.toString();
	}
	
	public String checkOemInfo(String info){
		if((new File("/oem/oem.prop")).exists()){
			try{
				return execForStringOutput(0,"cat /oem/oem.prop | grep "+info+" | cut -f2 -d =");
			} catch(Exception e){ return null; }
		}
		return null;
	}
	
}
