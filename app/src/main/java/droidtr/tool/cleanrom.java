package droidtr.tool;

import android.app.*;
import android.graphics.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import java.io.*;

import java.lang.Process;

/**
 * Created by a on 07.06.2017.
 */

public class cleanrom extends Activity {

	public boolean bbeyz = false;
	public boolean bkrmz = false;
	public boolean bkara = false;
	public String[] beyaz ={"/system/app/Books" , "/system/app/CalendarGooglePrebuilt" , "/system/app/CloudPrint2" , "/system/app/DMAgent" , "/system/app/Drive" , "/system/app/EditorsDocs" , "/system/app/EditorsSheets" , "/system/app/EditorsSlides" , "/system/app/FitnessPrebuilt" , "/system/app/GoogleCalendarSyncAdapter" , "/system/app/GoogleEars" , "/system/app/GoogleEarth" , "/system/app/GoogleHindiIME" , "/system/app/GoogleJapaneseInput" , "/system/app/GooglePinyinIME" , "/system/app/GoogleTTS" , "/system/app/GoogleZhuyinIME" , "/system/app/Hangouts" , "/system/app/KoreanIME" , "/system/app/Maps" , "/system/app/Newsstand" , "/system/app/PlayGames" , "/system/app/PlusOne" , "/system/app/PrebuiltBugle" , "/system/app/PrebuiltExchange3Google" , "/system/app/PrebuiltGmail" , "/system/app/PrebuiltKeep" , "/system/app/PrebuiltNewsWeather" , "/system/app/Street" , "/system/app/TranslatePrebuilt" , "/system/app/Tycho" , "/system/app/Videos" , "/system/app/Wallet" , "/system/app/WebViewGoogle" , "/system/app/YouTube" , "/system/app/talkback" , "/system/etc/permissions/com.google.android.maps.xml" , "/system/etc/permissions/com.google.widevine.software.drm.xml" , "/system/etc/sysconfig/whitelist_com.android.omadm.service.xml" , "/system/framework/com.google.android.maps.jar" , "/system/framework/com.google.widevine.software.drm.jar" , "/system/priv-app/ConfigUpdater" , "/system/priv-app/GCS" , "/system/priv-app/TagGoogle" , "/system/usr/srec/en-US/" , "/system/tts/"};
	public String[] kara={"/system/priv-app/Velvet" , "/system/priv-app/GoogleBackupTransport" , "/system/priv-app/GoogleFeedback" , "/system/priv-app/GoogleLoginService" , "/system/priv-app/GoogleOneTimeInitializer" , "/system/priv-app/GooglePackageInstaller" , "/system/priv-app/GooglePartnerSetup" , "/system/priv-app/GoogleServicesFramework" , "/system/priv-app/Phonesky" , "/system/priv-app/PrebuiltGmsCore" , "/system/etc/preferred-apps/google.xml" , "/system/etc/sysconfig/google.xml" , "/system/etc/sysconfig/google_build.xml" };
	public String[] kirmizi={"/system/app/Chrome" , "/system/app/GoogleCamera" , "/system/app/GoogleHome" , "/system/app/LatinImeGoogle" , "/system/app/Music2" , "/system/app/Photos" , "/system/app/CalculatorGoogle" , "/system/app/PrebuiltDeskClockGoogle" , "/system/priv-app/GoogleContacts" , "/system/priv-app/GoogleDialer" , "/system/priv-app/SetupWizard" , "/system/etc/permissions/com.google.android.camera2.xml" , "/system/etc/permissions/com.google.android.dialer.support.xml" , "/system/etc/permissions/com.google.android.media.effects.xml" , "/system/framework/com.google.android.camera.experimental2015.jar" , "/system/framework/com.google.android.camera2.jar" , "/system/framework/com.google.android.dialer.support.jar" , "/system/framework/com.google.android.media.effects.jar" , "/system/lib/libfilterpack_facedetect.so" , "/system/lib/libjni_keyboarddecoder.so" , "/system/lib/libjni_latinimegoogle.so" , "/system/etc/permissions/com.google.android.camera.experimental2015.xml" , "/system/app/CalendarGooglePrebuilt"};
	
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
		int p = 16;
        LinearLayout ly = new LinearLayout(this);
		ScrollView sv = new ScrollView(this);
		LinearLayout ll = new LinearLayout(this);
		ll.setOrientation(LinearLayout.HORIZONTAL);
		ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
		ll.setPadding(p,p,p,p);
		ll.setGravity(Gravity.CENTER_HORIZONTAL);
		ll.setBackgroundColor(getResources().getColor(R.color.arkaplan));
		ly.setBackgroundColor(getResources().getColor(R.color.arkaplan));
		sv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
		ly.setOrientation(LinearLayout.VERTICAL);
        ly.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        ly.setPadding(p,p+kitkat(),p,p);
		ly.addView(ll);
		sv.addView(ly);
        
        for (int i=0;i!=beyaz.length;i++){
            if (dogrula(beyaz[i])){
				bbeyz = true;
                Button b = new Button(this);
                b.setText(beyaz[i]);
                b.setBackgroundColor(Color.WHITE);
                b.setTextColor(Color.BLACK);
                ekle(b,beyaz[i],false,0);
                ly.addView(b);
            }
        }

        for (int i=0;i!=kirmizi.length;i++){
            if (dogrula(kirmizi[i])){
				bkrmz = true;
                Button b = new Button(this);
                b.setText(kirmizi[i]);
                b.setBackgroundColor(Color.RED);
                b.setTextColor(Color.WHITE);
                ekle(b,kirmizi[i],false,1);
                ly.addView(b);
            }
        }
        for (int i=0;i!=kara.length;i++){
            if (dogrula(kara[i])){
				bkara = true;
                Button b = new Button(this);
                b.setText(kara[i]);
                b.setBackgroundColor(Color.BLACK);
                b.setTextColor(Color.WHITE);
                ekle(b,kara[i],false,2);
                ly.addView(b);
            }
        }
		
		if(bbeyz){
			Button b = new Button(this);
			b.setText("Clear");
			b.setBackgroundColor(Color.WHITE);
			b.setTextColor(Color.BLACK);
			ekle(b,"",true,0);
			ll.addView(b);
		}
		
		if(bkrmz){
			Button b = new Button(this);
			b.setText("Clear");
			b.setBackgroundColor(Color.RED);
			b.setTextColor(Color.WHITE);
			ekle(b,"",true,1);
			ll.addView(b);
		}
		
		if(bkara){
			Button b = new Button(this);
			b.setText("Clear");
			b.setBackgroundColor(Color.BLACK);
			b.setTextColor(Color.WHITE);
			ekle(b,"",true,2);
			ll.addView(b);
		}
		
		if(!bbeyz && !bkrmz && !bkara){
			Button b = new Button(this);
			b.setText("All files are cleaned already");
			b.setBackgroundColor(Color.BLACK);
			b.setTextColor(Color.WHITE);
			b.setLayoutParams(new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT,
									LinearLayout.LayoutParams.WRAP_CONTENT));
			b.setEnabled(false);
			ll.addView(b);
		}
		
        ly.setVisibility(View.VISIBLE);
        ly.setBackgroundColor(Color.BLACK);
        setContentView(sv);

    }
	
	public int kitkat(){
		if(Build.VERSION.SDK_INT == 19) return 75;
		else return 0;
	}

    private void ekle(final Button b, final String s, final boolean clr, final int ct){
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		b.setLayoutParams(lp);
		if(clr) lp.weight = 0.33f;
		b.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
					if(clr){
						int i = 0;
						if(ct == 0){
							while(true){
								if(i>=beyaz.length){
									i = 0;
									break;
								} else {
									try{ new shell().exec(1,"rm -rf "+beyaz[i]);
									} catch(Exception e){}
									i++;
								}
							}
						} if(ct == 1){
							while(true){
								if(i>=kirmizi.length){
									i = 0;
									break;
								} else {
									try{ new shell().exec(1,"rm -rf "+kirmizi[i]);
									} catch(Exception e){}
									i++;
								}
							}
						} if(ct == 2){
							while(true){
								if(i>=kara.length){
									i = 0;
									break;
								} else {
									try{ new shell().exec(1,"rm -rf "+kara[i]);
									} catch(Exception e){}
									i++;
								}
							}
						}
						b.setVisibility(View.GONE);
					} else {
						try{
							b.setText("rm -rf "+s);
							while(true){
								if(!((new File(s)).exists())) break;
								else {
									new shell().exec(1,"rm -rf "+s);
									Thread.sleep(500);
								}
							}
							b.setVisibility(View.GONE);
						} catch(Exception e){}
					}
                }
            });
        }
	
		public boolean dogrula(String s){
			if(new shell().checkFile(s,0) == 0 
			|| new shell().checkFile(s+".apk",0) == 0) return true;
			else return false;
		}
}
